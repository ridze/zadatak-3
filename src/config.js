'use strict';

const startCoordinate = [1, 2];
const endCoordinate = [6, 7];
const blocks = 20;

module.exports = {
	startCoordinate,
	endCoordinate,
	blocks,
};
