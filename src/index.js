'use strict';

const data = require('./config');

let myMatrix;
const openList = [];
const closedList = [];
const currentPath = [];
const blockedFields = [];
const end = {
	x: data.endCoordinate[0],
	y: data.endCoordinate[1],
};
const start = {
	x: data.startCoordinate[0],
	y: data.startCoordinate[1],
};
const { blocks } = data;

class Field {
	constructor(x, y, blocked = false, evaluated = false) {
		this.x = x;
		this.y = y;
		this.blocked = blocked;
		this.evaluated = evaluated;
		this.H = this.countH();
	}

	countH() {
		return Math.abs(this.y - end.y) + Math.abs(this.x - end.x);
	}

	evaluate(parentField, openLst = openList) {
		if (!this.evaluated || parentField.G + 1 < this.G) {
			this.evaluated = true;
			this.G = parentField.G + 1;
			this.F = this.G + this.H;
			this.parentField = parentField;
			openLst.push(this);
		}
	}

	lookAround(matrix = myMatrix, openLst = openList, closedLst = closedList) {
		const checked = openLst.splice(openLst.indexOf(this), 1);
		closedLst.push(checked);
		const up = (this.y + 1 < matrix.length) ? matrix[this.x][this.y + 1] : false;
		const down = (this.y - 1 >= 0) ? matrix[this.x][this.y - 1] : false;
		const left = (this.x - 1 >= 0) ? matrix[this.x - 1][this.y] : false;
		const right = (this.x + 1 < matrix.length) ? matrix[this.x + 1][this.y] : false;
		if (up && up !== this.parentField && up.blocked !== true) {
			up.evaluate(this);
		}
		if (down && down !== this.parentField && down.blocked !== true) {
			down.evaluate(this);
		}
		if (left && left !== this.parentField && left.blocked !== true) {
			left.evaluate(this);
		}
		if (right && right !== this.parentField && right.blocked !== true) {
			right.evaluate(this);
		}
	}
}

const startField = new Field(start.x, start.y, false, true);
startField.G = 0; startField.F = startField.H;
const endField = new Field(end.x, end.y);
endField.G = startField.H; endField.F = endField.G;
openList.push(startField);

const generateMatrix = (n) => {
	const matrix = [];
	for (let i = 0; i < n; i += 1) {
		matrix[i] = [];
		for (let j = 0; j < n; j += 1) {
			matrix[i][j] = new Field(i, j);
		}
	}
	return matrix;
};

const resetMatrix = (matrix) => {
	for (let i = 0; i < matrix.length; i += 1) {
		for (let j = 0; j < matrix.length; j += 1) {
			matrix[i][j].evaluated = false;
		}
	}
};

const findLowestF = (openLst = openList) => {
	let lowestF = openLst[0];
	for (let i = 1; i < openLst.length; i += 1) {
		if (openLst[i].F < lowestF.F) {
			lowestF = openLst[i];
		} else if (openLst[i].F === lowestF.F) {
			if (openLst[i].H < lowestF.H) lowestF = openLst[i];
		}
	}
	return lowestF;
};

const findPath = (openLst = openList, closedLst = closedList) => {
	resetMatrix(myMatrix);
	openLst.splice(0, openLst.length);
	closedLst.splice(0, closedLst.length);
	openLst.push(startField);
	startField.evaluated = true;
	let nextField = findLowestF();
	while (openLst.length && nextField.H > 0) {
		nextField.lookAround();
		nextField = findLowestF();
	}
	if (nextField === endField) {
		const path = [];
		path.push(nextField);
		while (nextField !== startField) {
			nextField = nextField.parentField;
			path.push(nextField);
		}
		return path;
	} return false;
};

const isTrapped = (testBlock, currentPth = currentPath) => {
	if (!testBlock.blocked && testBlock !== startField && testBlock !== endField) {
		if (currentPth.indexOf(testBlock) !== -1) {
			testBlock.blocked = true;
			const path = findPath();
			if (path) {
				currentPth.splice(0, currentPth.length);
				for (let i = 0; i < path.length; i += 1) {
					currentPth.push(path[i]);
				}
				return false;
			}
			testBlock.blocked = false;
			return true;
		}
		testBlock.blocked = true;
		return false;
	}
	return true;
};

const blockGenerator = (matrix = myMatrix, blockedFlds = blockedFields) => {
	let newBlock;
	do {
		const x = Math.floor(Math.random() * matrix[0].length);
		const y = Math.floor(Math.random() * matrix[0].length);
		newBlock = matrix[x][y];
	}
	while (isTrapped(newBlock));
	newBlock.blocked = true;
	blockedFlds.push(newBlock);
};

myMatrix = generateMatrix(10);

myMatrix[startField.x][startField.y] = startField;
myMatrix[endField.x][endField.y] = endField;

// default shortest path (in reverse order) on matrix which has no blocks:
for (let i = end.x; i !== start.x; (end.x < start.x ? i += 1 : i -= 1)) {
	currentPath.push(myMatrix[i][end.y]);
}
for (let i = end.y; i !== start.y; (end.y < start.y ? i += 1 : i -= 1)) {
	currentPath.push(myMatrix[start.x][i]);
}
currentPath.push(myMatrix[start.x][start.y]);

for (let i = 1; i <= blocks; i += 1) {
	blockGenerator();
}

console.log('\nBLOCKED FIELDS: \n');
for (let i = 0; i < blockedFields.length; i += 1) console.log('X: ', blockedFields[i].x, '| Y: ', blockedFields[i].y);
console.log('\nPATH: \n');
for (let i = currentPath.length - 1; i >= 0; i -= 1) console.log('X: ', currentPath[i].x, '| Y: ', currentPath[i].y);
